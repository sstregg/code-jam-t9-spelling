﻿using System;
using System.IO;
using CodeJam.Contests.Spelling;

namespace CodeJam.Main
{
    class Program
    {
        static void Main ( string[] args )
        {
            if ( args.Length == 0 )
            {
                WorkWithUserInput();
            }
            else
            {
                WorkWithFileInput( args[0] );
            }
        }

        static void WorkWithUserInput ( )
        {
            while ( true )
            {
                Console.WriteLine(
@"Usage:
1. On the first line input number of cases and press enter;
2. Sequentially enter messages N times (N - number of cases) and press enter after each time.

Input:" );
                try
                {
                    var spelling =  new T9Spelling( Console.OpenStandardInput() );

                    Console.WriteLine( "\r\nOutput:\r\n{0}\r\n\r\nEnd", string.Join( "\r\n", spelling.Output ) );
                    Console.ReadKey();
                    Console.Clear();
                }
                catch ( DataValidationFailedException ex )
                {
                    if ( !HandleError( ex.Message ) )
                        break;
                }
                catch ( EndOfStreamException ex )
                {
                    if ( !HandleError( ex.Message ) )
                        break;
                }
            }
        }

        static bool HandleError ( string errorText )
        {
            Console.WriteLine( "\r\nError: {0}\r\nRepeat? [Y/N]", errorText );
            var info = Console.ReadKey();
            var result = char.ToUpper( info.KeyChar ) == 'Y';
            Console.Clear();

            return result;
        }

        static void WorkWithFileInput ( string filePath )
        {
            while ( true )
            {
                try
                {
                    Console.Write( "Openning file '{0}' for reading... ", filePath );
                    using ( var fileStream = File.Open( filePath, FileMode.Open, FileAccess.Read ) )
                    {
                        var spelling = new T9Spelling( fileStream );

                        Console.WriteLine( "OK" );
                        Console.WriteLine( "Number of cases: " + spelling.NumberOfCases );
                        Console.WriteLine( "Input messages:\r\n" + string.Join( "\r\n", spelling.Input ) );
                        Console.WriteLine( "Output:\r\n" + string.Join( "\r\n", spelling.Output ) );
                    }

                    Console.ReadKey();
                    break;
                }
                catch ( DataValidationFailedException ex )
                {
                    if ( !HandleError( ex.Message ) )
                        break;
                }
                catch ( EndOfStreamException ex )
                {
                    if ( !HandleError( ex.Message ) )
                        break;
                }
                catch ( IOException ex )
                {
                    if ( !HandleError( @"Input\output exception occurred: " + ex.Message ) )
                        break;
                }
            }
        }
    }
}
