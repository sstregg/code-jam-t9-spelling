﻿using System.IO;
using System.Text;

namespace CodeJam.Contests.Tests.Extensions
{
    internal static class StringExtensions
    {
        public static Stream ToStream ( this string value, Encoding encoding = null )
        {
            return new MemoryStream( encoding.GetBytes( value ) ) { Position = 0 };
        }
    }
}
