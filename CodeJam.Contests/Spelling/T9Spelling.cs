﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace CodeJam.Contests.Spelling
{
    /// <summary>
    /// Provides solution for "T9 Spelling" task from "Google Code Jam" contest.
    /// </summary>
    public class T9Spelling
    {
        #region ~ Constants ~

        /// <summary>
        /// The minimum number of input cases.
        /// </summary>
        public const int MinimumInputCases = 1;
        /// <summary>
        /// The maximum number of input cases.
        /// </summary>
        public const int MaximumInputCases = 100;
        /// <summary>
        /// The minimum length of input message.
        /// </summary>
        public const int MinimumInputMessageLength = 1;
        /// <summary>
        /// The maximum length of input message.
        /// </summary>
        public const int MaximumInputMessageLength = 1000;

        #endregion

        #region ~ Static ~

        /// <summary>
        /// Stores supported letters and related keystrokes sequences.
        /// </summary>
        private static readonly IDictionary<char, string> mKeystrokesSequenceToLetterMapping;

        /// <summary>
        /// Initialize static variables.
        /// </summary>
        static T9Spelling ( )
        {
            mKeystrokesSequenceToLetterMapping = GetKeystrokesSequenceToLetterMapping();
        }

        /// <summary>
        /// Returns a dictionary, where the key is a letter, and the value is a sequence of keystrokes.
        /// </summary>
        private static IDictionary<char, string> GetKeystrokesSequenceToLetterMapping ( )
        {
            const int LatinAlphabetCount = 26;
            // set capacity to LatinAlphabetCount + space char
            var result = new Dictionary<char, string>( LatinAlphabetCount + 1 )
            {
                {' ', "0"}
            };

            // starting with the second digit, the first is not used
            char digit = '2';
            for ( int i = 0; i < LatinAlphabetCount; )
            {
                var letter = (char)('a' + i);
                // on each digit, except 7 and 9, we have 3 letters. On 7 and 9 we have 4 letters
                var letterCountOnDigit = (digit == '7' || digit == '9') ? 4 : 3;

                // add each letter and it sequence of digit keystrokes to dictionary
                for ( int j = 0; j < letterCountOnDigit; j++ )
                    result.Add( (char)(letter + j), new string( digit, j + 1 ) );

                digit++;
                i += letterCountOnDigit;
            }

            return result;
        }

        #endregion

        #region ~ .ctors ~

        /// <summary>
        /// Initialize an instance of the <see cref="T9Spelling"/> class for the specified stream on the UTF8 character encoding, and optionally leaves the stream open.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        /// <param name="leaveOpen">true to leave the stream open after <see cref="T9Spelling"/> initialized; otherwise, false.</param>
        public T9Spelling ( Stream stream, bool leaveOpen = true )
        {
            if ( stream == null )
                throw new ArgumentNullException( "stream" );

            ProcessStream( stream, Encoding.UTF8, leaveOpen );
        }

        /// <summary>
        /// Initialize an instance of the <see cref="T9Spelling"/> class for the specified stream on the specified character encoding, and optionally leaves the stream open.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <param name="leaveOpen">true to leave the stream open after <see cref="T9Spelling"/> initialized; otherwise, false.</param>
        /// <exception cref="ArgumentNullException">Thrown if stream is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if encoding is null.</exception>
        /// <exception cref="DataValidationFailedException">Thrown if the input data has invalid data.</exception>
        /// <exception cref="EndOfStreamException">Thrown if end of file is reached unexpectedly while reading the stream.</exception>
        public T9Spelling ( Stream stream, Encoding encoding, bool leaveOpen = true )
        {
            if ( stream == null )
                throw new ArgumentNullException( "stream" );
            if ( encoding == null )
                throw new ArgumentNullException( "encoding" );

            ProcessStream( stream, encoding, leaveOpen );
        }

        #endregion

        #region ~ Methods ~

        /// <summary>
        /// Processing the specified stream on the specified character encoding, and optionally leaves the stream open. 
        /// After processing, intialize properties <see cref="NumberOfCases"/>, <see cref="Input"/> and <see cref="Output"/>.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <param name="leaveOpen">true to leave the stream open after use; otherwise, false.</param>
        private void ProcessStream ( Stream stream, Encoding encoding, bool leaveOpen )
        {
            using ( var reader = new StreamReader( stream, encoding, true, 4096, leaveOpen ) )
            {
                if ( reader.EndOfStream )
                    throw new EndOfStreamException( "Cannot read the value of the number of cases, because the stream unexpectedly ended." );

                var numberOfCases = ParseNumberOfCases( reader.ReadLine() );
                var input = new string[numberOfCases];
                var output = new string[numberOfCases];

                for ( int i = 0; i < numberOfCases; i++ )
                {
                    if ( reader.EndOfStream )
                        throw new EndOfStreamException( string.Format( "Cannot read message, because the stream unexpectedly ended. Expected file lines count is {0}, actual is {1}.", numberOfCases, i ) );

                    string message = reader.ReadLine();
                    string keystrokesSequences = ParseInputMessage( message );

                    input[i] = message;
                    output[i] = string.Format( "Case #{0}: {1}", i + 1, keystrokesSequences );
                }

                NumberOfCases = numberOfCases;
                Input = input;
                Output = output;
            }
        }

        /// <summary>
        /// Parse the number of cases.
        /// </summary>
        /// <param name="rawValue">String representing the number of cases.</param>
        /// <returns>Returns an integer representing parsed number of cases.</returns>
        private int ParseNumberOfCases ( string rawValue )
        {
            int numberOfCases;
            // parse only integers without sign symbol (0, 001, 3, 99, etc.)
            if ( !int.TryParse( rawValue, NumberStyles.None, null, out numberOfCases ) )
                throw new DataValidationFailedException( string.Format( "The value of a number of cases is not an integer: '{0}'.", rawValue ) );
            if ( !(numberOfCases >= MinimumInputCases && numberOfCases <= MaximumInputCases) )
                throw new DataValidationFailedException(string.Format( "The value of a number of cases should be between {0} and {1}.", MinimumInputCases, MaximumInputCases ) );

            return numberOfCases;
        }

        /// <summary>
        /// Parse input message to keystrokes sequences.
        /// </summary>
        /// <param name="rawValue"></param>
        /// <returns>Returns string representing keystrokes sequences.</returns>
        private string ParseInputMessage ( string rawValue )
        {
            if ( !(rawValue.Length >= MinimumInputMessageLength && rawValue.Length <= MaximumInputMessageLength) )
                throw new DataValidationFailedException(string.Format( "Input message should have length between {0} and {1} characters.", MinimumInputMessageLength, MaximumInputMessageLength ) );

            var keystrokesSequences = new StringBuilder();
            var prevDigit = '\0';

            for ( int i = 0; i < rawValue.Length; i++ )
            {
                var ch = rawValue[i];

                if ( !IsValidLetter( ch ) )
                    throw new DataValidationFailedException( string.Format(
                        "Input string '{0}' contains invalid character at position {1}. " +
                        "Input string must consist of only lowercase characters a-z and space characters ' '.", rawValue, i + 1 ) );

                // the first char of keystrokesSequence is a digit on keypad
                // example: if a keystroke is "555" (L letter), then the digit will be '5'
                var keystrokesSequence = mKeystrokesSequenceToLetterMapping[ch];

                if ( keystrokesSequence[0] == prevDigit )
                    keystrokesSequences.Append( ' ' );

                keystrokesSequences.Append( keystrokesSequence );
                prevDigit = keystrokesSequence[0];
            }

            return keystrokesSequences.ToString();
        }

        /// <summary>
        /// Returns true, if <see cref="letter"/> belongs to lowercase characters range (a-z) or if it equals to space character ' '.
        /// </summary>
        /// <param name="letter">A letter for check.</param>
        private bool IsValidLetter ( char letter )
        {
            return mKeystrokesSequenceToLetterMapping.ContainsKey( letter );
        }

        #endregion

        #region ~ Properties ~

        /// <summary>
        /// Gets the count of input and output cases.
        /// </summary>
        public int NumberOfCases { get; private set; }

        /// <summary>
        /// Gets input messages enumeration.
        /// </summary>
        public IEnumerable<string> Input { get; private set; }

        /// <summary>
        /// Gets processed input messages enumeration.
        /// </summary>
        public IEnumerable<string> Output { get; private set; }

        #endregion
    }
}
