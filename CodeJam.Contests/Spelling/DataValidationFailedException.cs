﻿using System;
using System.Runtime.Serialization;

namespace CodeJam.Contests.Spelling
{
    /// <summary>
    /// Provides information about error during data validation.
    /// </summary>
    [Serializable]
    public class DataValidationFailedException : Exception
    {
        internal DataValidationFailedException ( string message ) : base( message ) { }

        internal DataValidationFailedException ( string message, Exception innerException ) : base( message, innerException ) { }

        internal DataValidationFailedException ( SerializationInfo info, StreamingContext context ) : base( info, context ) { }
    }
}
