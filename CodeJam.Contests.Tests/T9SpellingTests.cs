﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using CodeJam.Contests.Spelling;
using CodeJam.Contests.Tests.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodeJam.Contests.Tests
{
    [TestClass]
    public class T9SpellingTests
    {
        static readonly Encoding CurrentEncoding = Encoding.UTF8;

        public TestContext TestContext { get; set; }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "InputOutput_Test.csv", DataAccessMethod.Random )]
        public void InputOutput_WithDefaultEncoding_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];
            var numberOfCasesExpected = int.Parse( input.Split( '\n' ).First().Trim() );
            var inputExpected = input.Split( '\n' ).Skip( 1 ).Select( x => x.Trim() ).ToArray();
            var outputExpected = ((string)TestContext.DataRow["expected"]).Split( '\n' ).Select( x => x.Trim() ).ToArray();

            var t9spelling =  new T9Spelling( input.ToStream( Encoding.UTF8 ) );

            var numberOfCasesActual = t9spelling.NumberOfCases;
            var inputActual = t9spelling.Input.ToArray();
            var outputActual = t9spelling.Output.ToArray();

            Assert.AreEqual( numberOfCasesExpected, numberOfCasesActual, "Expected number of cases is '{0}', but actual is '{1}'", numberOfCasesExpected, numberOfCasesActual );

            for ( int i = 0; i < numberOfCasesExpected; i++ )
                Assert.AreEqual( inputExpected[i], inputActual[i], "Expected input case #{0} is '{1}', but actual is '{2}'", i + 1, inputExpected[i], inputActual[i] );
            for ( int i = 0; i < numberOfCasesExpected; i++ )
                Assert.AreEqual( outputExpected[i], outputActual[i], "Expected output case #{0} is '{1}', but actual is '{2}'", i + 1, outputExpected[i], outputActual[i] );
        }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "InputOutput_Test.csv", DataAccessMethod.Random )]
        public void InputOutput_WithCustomEncoding_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];
            var countExpected = int.Parse( input.Split( '\n' ).First().Trim() );
            var inputExpected = input.Split( '\n' ).Skip( 1 ).Select( x => x.Trim() ).ToArray();
            var outputExpected = ((string)TestContext.DataRow["expected"]).Split( '\n' ).Select( x => x.Trim() ).ToArray();

            var t9spelling =  new T9Spelling( input.ToStream( Encoding.Default ), Encoding.Default );

            var numberOfCasesActual = t9spelling.NumberOfCases;
            var inputActual = t9spelling.Input.ToArray();
            var outputActual = t9spelling.Output.ToArray();

            Assert.AreEqual( countExpected, numberOfCasesActual, "Expected number of cases is '{0}', but actual is '{1}'", countExpected, numberOfCasesActual );

            for ( int i = 0; i < countExpected; i++ )
                Assert.AreEqual( inputExpected[i], inputActual[i], "Expected input case #{0} is '{1}', but actual is '{2}'", i + 1, inputExpected[i], inputActual[i] );
            for ( int i = 0; i < countExpected; i++ )
                Assert.AreEqual( outputExpected[i], outputActual[i], "Expected output case #{0} is '{1}', but actual is '{2}'", i + 1, outputExpected[i], outputActual[i] );
        }

        [TestMethod]
        public void NullStream_Test ( )
        {
            try
            {
                new T9Spelling( null );
                Assert.Fail( "T9Spelling initialization with null stream: An ArgumentNullException should have been thrown!" );
            }
            catch ( ArgumentNullException ex )
            {
                Assert.AreEqual( "stream", ex.ParamName );
            }

            try
            {
                new T9Spelling( null, Encoding.Default );
                Assert.Fail( "T9Spelling initialization with null stream and specified encoding: An ArgumentNullException should have been thrown!" );
            }
            catch ( ArgumentNullException ex )
            {
                Assert.AreEqual( "stream", ex.ParamName );
            }
        }

        [TestMethod]
        public void NullEncoding_Test ( )
        {
            try
            {
                new T9Spelling( Stream.Null, null );
                Assert.Fail( "T9Spelling initialization with specified stream and null encoding: An ArgumentNullException should have been thrown!" );
            }
            catch ( ArgumentNullException ex )
            {
                Assert.AreEqual( "encoding", ex.ParamName );
            }
        }

        [TestMethod]
        [ExpectedException( typeof( EndOfStreamException ), "Unexpected end of stream. Cannot read value of number of cases." )]
        public void NumberOfCases_EOF_Test ( )
        {
            var input = string.Empty;

            new T9Spelling( input.ToStream( CurrentEncoding ), CurrentEncoding );
        }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "NumberOfCases_NAN_Test.csv", DataAccessMethod.Random )]
        public void NumberOfCases_NAN_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];

            try
            {
                new T9Spelling( input.ToStream( CurrentEncoding ), CurrentEncoding );
                Assert.Fail( "A DataValidationFailedException should have been thrown!" );
            }
            catch ( DataValidationFailedException ex )
            {
                Assert.AreEqual( (string)TestContext.DataRow["expected"], ex.Message );
            }
        }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "NumberOfCases_OutOfRange_Test.csv", DataAccessMethod.Random )]
        public void NumberOfCases_OutOfRange_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];

            try
            {
                new T9Spelling( input.ToStream( CurrentEncoding ), CurrentEncoding );
                Assert.Fail( "A DataValidationFailedException should have been thrown!" );
            }
            catch ( DataValidationFailedException ex )
            {
                Assert.AreEqual(
                    string.Format( "The value of a number of cases should be between {0} and {1}.", T9Spelling.MinimumInputCases, T9Spelling.MaximumInputCases ),
                    ex.Message );
            }
        }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "ParsingCases_UnexpectedEOF_Test.csv", DataAccessMethod.Random )]
        public void ParsingCases_UnexpectedEOF_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];

            try
            {
                new T9Spelling( input.ToStream( CurrentEncoding ), CurrentEncoding );
                Assert.Fail( "An EndOfStreamException should have been thrown!" );
            }
            catch ( EndOfStreamException ex )
            {
                Assert.AreEqual( (string)TestContext.DataRow["expected"], ex.Message );
            }
        }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "ParsingCases_TextLengthOutOfRange_Test.csv", DataAccessMethod.Random )]
        public void ParsingCases_TextLengthOutOfRange_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];

            try
            {
                new T9Spelling( input.ToStream( CurrentEncoding ), CurrentEncoding );
                Assert.Fail( "A DataValidationFailedException should have been thrown!" );
            }
            catch ( DataValidationFailedException ex )
            {
                Assert.AreEqual(
                    string.Format( "Input message should have length between {0} and {1} characters.", T9Spelling.MinimumInputMessageLength, T9Spelling.MaximumInputMessageLength ),
                    ex.Message );
            }
        }

        [TestMethod]
        [DataSource( "Microsoft.VisualStudio.TestTools.DataSource.CSV", @"Resources\Data\", "ParsingCases_InvalidData_Test.csv", DataAccessMethod.Random )]
        public void ParsingCases_InvalidData_Test ( )
        {
            var input = (string)TestContext.DataRow["input"];

            try
            {
                new T9Spelling( input.ToStream( CurrentEncoding ), CurrentEncoding );
                Assert.Fail( "An InvalidDataException should have been thrown!" );
            }
            catch ( DataValidationFailedException ex )
            {
                Assert.AreEqual( (string)TestContext.DataRow["expected"], ex.Message );
            }
        }
    }
}
